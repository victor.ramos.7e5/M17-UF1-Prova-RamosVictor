using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    public GameObject naves;
    public GameObject navegande;
    public float timespawn;
    public float repeatSpawn;


    public Transform xRangeLeft;
    public Transform xRangeright;

    public Transform yRangeup;
    public Transform yRangedown;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawnnaves", timespawn, repeatSpawn);
        InvokeRepeating("SpawnNavegrande", 1, 10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawnnaves()
    {
        Vector3 spawnpos = new Vector3(0, 0, 0);
        spawnpos = new Vector3(Random.Range(xRangeLeft.position.x, xRangeright.position.x), yRangeup.position.y, 0);
        GameObject fireballs = Instantiate(naves, spawnpos, gameObject.transform.rotation);
      
    }
    public void SpawnNavegrande()
    {
        var instance = Instantiate(navegande, new Vector3(Random.Range(xRangeLeft.position.x, xRangeright.position.x), yRangeup.position.y, 0), Quaternion.identity);
        instance.transform.up = Vector2.up;
    }

}
