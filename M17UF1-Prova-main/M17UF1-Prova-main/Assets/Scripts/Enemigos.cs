using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigos : MonoBehaviour
{
    public GameObject nave;
    [SerializeField]
    private GameObject bullet;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        CheckIfOut();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        Destroy(nave);
    }
  

    void CheckIfOut()
    {
        if (transform.position.y < -4)
        {
            Destroy(gameObject);
        }
    }


}
