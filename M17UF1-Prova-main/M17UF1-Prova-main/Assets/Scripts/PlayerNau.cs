using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNau : MonoBehaviour
{
    public GameObject bala;
    [SerializeField]
    private bool spaceInput;
    private bool canShoot;
    public float speed;
    public float fuerzadisp;

    public int numrafaga;
    // Start is called before the first frame update
    void Start()
    {
        canShoot = true;
    }

    // Update is called once per frame
    void Update()
    {
       
        spaceInput = Input.GetKey(KeyCode.Space);
        Movimiento();
        Disparar();
    }
    public void Movimiento()
    {
        float imputMovHori = Input.GetAxis("Horizontal");

        transform.position = new Vector3(speed * imputMovHori + transform.position.x, speed * Input.GetAxis("Vertical") + transform.position.y, transform.position.z);
    }

    public void Disparar()
    {
        if (canShoot)
        {
            if (spaceInput)
            {
                var disparos = Instantiate(bala, transform.position, Quaternion.identity);
                disparos.GetComponent<Rigidbody2D>().AddForce(Vector2.up * fuerzadisp, ForceMode2D.Impulse);
                disparos.transform.up = Vector2.down;
                StartCoroutine(ShootCooldown());
            }

        }
    }
    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(0.15f);
        numrafaga++;
        if (numrafaga >= 5)
        {
            yield return new WaitForSeconds(0.5f);
            numrafaga = 0;
        }
        canShoot = true;
    }
}
